# Raag Recognition


---

### Introduction

This project aims to classify North Indian Classical Ragas using deep neural networks. We try to classify recordings into 3 ragas by using Convolutional networks and Convolutional Recurrent networks. We have used the Dunya Dataset for Hindustani music which provides pre calculated features. Of these we use pitch values and tonic of the recordings for our models.
We have used 3 models for this purpose: `CNN1Layer`, `CNN2Layer`, `CRNN1Layer`. The block size of the pitch values that we fed to the network was of 10 sec.

---

### Dependencies:

- pytorch
- numpy
- matplotlib


---

### Files and Folder Structure:

* `data` folder stores the raw dataset and the pickled processed dataset.
* `model` folder saves the latest trained model.
* `code` folder contains the `datautils` and `network` folders
	* `datautils`:
		* `dataloader.py` contains the dataset preparing & preprocessing methods.
	* `network`:
		* `network.py` contains defined models.	
		* `networkutils.py` contains train, evaluate, visualization and model loading & saving methods.
		* `trainer.py` is the actual script that you'll use to prepare datasets and train the models.
		* `tester.py` is similar to the trainer script but works on the dataset in the `test` folder.

---

### Dataset:

* Download the [data folder](https://drive.google.com/open?id=1pGhermFFwZNXwtQJOunX3aU5aUOdlpvd). Place the `data/` folder at the root of the cloned repository (`raag-recognition/data/`). 


### Instructions to run the models:
	
We have 3 models `CNN1Layer`, `CNN2Layer`, `CRNN1Layer`. The following commands will help you to train and test all the models. The models won't take much time to run without GPU if you're using `--pitch-block-length 10` (10 sec block). The CRNN may take more time if you're using a 30 sec block so a GPU can be used in this case. 


Important flags:

* `--model` to pass model that you want to run. (Default: CNN2Layer)
* `--pitch-block-length` to set block length (in seconds). (Default: 10). Can try with 30. 
* `--augment-data` to use data augmentation when preparing data.
* `--prepare-dataset` to prepare dataset.
* `--epochs` (Default: 50)
* `--learning-rate` (Default: 1e-2)
	
	
	
####There are 2 steps involved: 

####1) Preparing the dataset:

- Block size 10 sec, Without Augmentation:
	* `python trainer.py --prepare-dataset`
or
- Block size 10 sec, With augmentation:
	* `python trainer.py --prepare-dataset --augment-data`
		
####2) Training:
	
- 1 layer CNN model:
	* `python trainer.py --model CNN1Layer`
or
- 2 layer CNN model:
	* `python trainer.py --model CNN2Layer`
or				
- CRNN model:				
	* `python trainer.py --model CRNN1Layer`
		
		
Note: The `trainer.py` script will split the dataset into train, val and test sets. If you want to test on completely different data the `tester.py` script can be used. This will use the data present in the `data/test/` folder.

Example commands with different configurations:

* `python trainer.py --pitch-block-length 10 --prepare-data --augment-data`
* `python trainer.py --pitch-block-length 10 --model CRNN1Layer --epochs 150 --learning-rate 1e-3`

* `python tester.py --pitch-block-length 30 --prepare-data`
* `python tester.py --pitch-block-length 30`

---

### Authors:

Kaushal Sali, Tejas Rode.