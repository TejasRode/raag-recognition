import torch
import torch.nn as nn
import argparse
import pickle
import networkutils
import sys
sys.path.append("../datautils")
import dataloader


parser = argparse.ArgumentParser(description='Tester')
parser.add_argument('--model', type=str, default='model')
parser.add_argument('--no-cuda', action='store_true', default=False, help='disables CUDA training')
parser.add_argument('--prepare-dataset', action='store_true', default=False)
parser.add_argument('--batch-size', type=int, default=32)
parser.add_argument('--augment-data', action='store_true', default=False)
parser.add_argument('--pitch-block-length', type=int, default=10, help='time duration in sec')
parser.add_argument('--same-pitch-threshold', type=int, default=225*2, help='threshold of consecutive pitches before discarding when building a block')
args = parser.parse_args()

args.cuda = not args.no_cuda and torch.cuda.is_available()
print('Running on GPU: ', args.cuda)

pitch_block_size = 225 * args.pitch_block_length

split = {'train': 0.0, 'val': 0.0, 'test': 1.0}

if args.prepare_dataset:
    print("Preparing Datasets...")
    dataset, ragadict = dataloader.prepareDataset(pitch_block_size=pitch_block_size,
                                                  split=split,
                                                  same_pitch_threshold=args.same_pitch_threshold,
                                                  augment_data=args.augment_data,
                                                  path='../../data/test/test_dataset')
    pickle.dump(dataset, open("../../data/test/pickled_test_dataset/dataset.pkl", "wb"))
    pickle.dump(ragadict, open("../../data/test/pickled_test_dataset/ragadict.pkl", "wb"))
    print("\nDatasets prepared & pickled.")
    quit()

print("Loading Pickled Data....")
dataset = pickle.load(open("../../data/test/pickled_test_dataset/dataset.pkl", "rb"))
ragadict = pickle.load(open("../../data/test/pickled_test_dataset/ragadict.pkl", "rb"))
print("Pickled Data Loaded.")


print("Preparing Data Loaders....")
_, _, test_data_loader = dataloader.prepareDataLoaders(dataset, args.batch_size)
print("Data Loaders prepared.")


model = networkutils.loadModel(args.model)

if args.cuda:
    model.cuda()

cross_entropy_loss = nn.CrossEntropyLoss()

print("\n----TESTING----")
test_loss, test_accuracy = networkutils.evaluate(model=model,
                                  data_loader=test_data_loader,
                                  loss_criterion=cross_entropy_loss)
print("Test loss:\t" + str(test_loss))
print("Test accuracy:\t" + str(test_accuracy))



