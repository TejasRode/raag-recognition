import os
import torch
from torch.autograd import Variable
import numpy as np
from matplotlib import pyplot as plt


def train(model, data_loader, loss_criterion, optimizer):
    model.train()
    total_loss = 0
    correct = 0
    n_examples = 0
    for i, batch in enumerate(data_loader):
        input, target = Variable(batch[0]), Variable(batch[1])
        input = input.reshape(input.shape[0], 1, input.shape[1])
        if torch.cuda.is_available():
            input = input.cuda()
            target = target.cuda()
        predictions, hidden_layer_outputs = model(input)   #For vis conv layers
        #predictions = model(input)
        loss = loss_criterion(predictions, target)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        total_loss += loss.item()
        predicted_raga = predictions.data.max(1, keepdim=True)[1]
        correct += predicted_raga.eq(target.data.view_as(predicted_raga)).cpu().sum()
        n_examples += predicted_raga.size(0)
    avg_loss = total_loss / len(data_loader)  # len(data_loader) gives total no of batches
    accuracy = 100. * float(correct) / n_examples

    # For visualization of layers and outputs
    #visualizeConvOutputs(hidden_layer_outputs[0])
    #visualizeConvWeights(model.convlayer1)
    #print('\nConv1 Weights:')
    #printConvMinMaxWeights(model.convlayer1)
    #printConvMinMaxWeights(model.convlayer2)

    print("Correct/Total:", correct.numpy(), '/', n_examples)
    return avg_loss, accuracy


def evaluate(model, data_loader, loss_criterion):
    model.eval()
    total_loss = 0
    correct = 0
    n_examples = 0
    for i, batch in enumerate(data_loader):
        #print("Processing batch " + str(i))
        input, target = Variable(batch[0]), Variable(batch[1])
        input = input.reshape(input.shape[0], 1, input.shape[1])
        if torch.cuda.is_available():
            input = input.cuda()
            target = target.cuda()
        predictions, hidden_layer_outputs = model(input)
        loss = loss_criterion(predictions, target)
        total_loss += loss.item()
        predicted_raga = predictions.data.max(1, keepdim=True)[1]
        correct += predicted_raga.eq(target.data.view_as(predicted_raga)).cpu().sum()
        n_examples += predicted_raga.size(0)
    avg_loss = total_loss / len(data_loader)
    accuracy = 100. * float(correct) / n_examples
    print("Correct/Total:", correct.numpy(), '/', n_examples)
    return avg_loss, accuracy


def visualizeConvOutputs(conv):
    plt.title("Conv Outputs")
    for i in range(8):
        plt.subplot(8, 1, i+1)
        plt.plot(conv.detach().numpy()[i])
    plt.tight_layout()
    plt.show()


def visualizeConvWeights(layer):
    plt.title("Conv Weights")
    for i in range(8):
        plt.subplot(8, 1, i+1)
        plt.plot(layer.weight.detach().numpy()[i][0])
    plt.tight_layout()
    plt.show()


def printConvMinMaxWeights(layer):
    for i in range(8):
        print('Filter', i+1, ':\tMax:', np.max(layer.weight.detach().numpy()[i][0]), '\tMin:', np.min(np.abs(layer.weight.detach().numpy()[i][0])))
    print()


def saveModel(model, path="../../model"):
    if not os.path.exists(path):
        os.makedirs(path)
    path += "/model.pt"
    torch.save(model, path)


def loadModel(model='model'):
    path = "../../model/" + model + '.pt'
    return torch.load(path)


def plotLoss(train_losses, val_losses):
    n = np.arange(len(train_losses))
    plt.xlabel("Number of epochs")
    plt.ylabel("Cross Entropy Loss")
    plt.plot(n, train_losses, label="train_loss")
    plt.plot(n, val_losses, label="val_loss")
    plt.legend()
    plt.savefig('../../results/loss.png')
    plt.savefig('../../results/loss.svg')
    plt.show()



