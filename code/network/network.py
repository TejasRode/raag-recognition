import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable


class CNN1Layer(nn.Module):

    def __init__(self, conv1_config, maxpool1_config, fc_layer_size, output_layer_size):
        super(CNN1Layer, self).__init__()
        # Layer 1
        self.convlayer1 = nn.Conv1d(in_channels=conv1_config['in_channels'],
                                   out_channels=conv1_config['out_channels'],
                                   kernel_size=conv1_config['kernel'],
                                   padding=conv1_config['padding'],
                                   stride=conv1_config['stride'])
        #self.conv1_bn = nn.BatchNorm2d(conv1_config['out_channels'])
        self.maxpoollayer1 = nn.MaxPool1d(kernel_size=maxpool1_config['kernel'],
                                         stride=maxpool1_config['stride'])

        self.relu1 = nn.ReLU()

        # Output Layer
        self.output_layer = nn.Linear(fc_layer_size, output_layer_size)


    def forward(self, audio):
        conv1 = self.convlayer1(audio)
        #conv1_bn = self.conv1_bn(conv1)
        relu1 = self.relu1(conv1)
        relu1 = F.dropout(relu1, 0.5)
        max1 = self.maxpoollayer1(relu1)
        max1 = max1.view(-1, max1.shape[1] * max1.shape[2])
        output_scores = self.output_layer(max1)

        conv2 = conv1
        return output_scores, (conv1)





class CNN2Layer(nn.Module):

    def __init__(self, conv1_config, maxpool1_config, conv2_config, maxpool2_config, fc_layer_size, output_layer_size):
        super(CNN2Layer, self).__init__()
        # Layer 1
        self.convlayer1 = nn.Conv1d(in_channels=conv1_config['in_channels'],
                                    out_channels=conv1_config['out_channels'],
                                    kernel_size=conv1_config['kernel'],
                                    padding=conv1_config['padding'],
                                    stride=conv1_config['stride'])
        self.maxpoollayer1 = nn.MaxPool1d(kernel_size=maxpool1_config['kernel'], stride=maxpool1_config['stride'])
        self.relu1 = nn.ReLU()

        # Layer 2
        self.convlayer2 = nn.Conv1d(in_channels=conv2_config['in_channels'],
                                    out_channels=conv2_config['out_channels'],
                                    kernel_size=conv2_config['kernel'],
                                    padding=conv2_config['padding'],
                                    stride=conv2_config['stride'])
        self.maxpoollayer2 = nn.MaxPool1d(kernel_size=maxpool2_config['kernel'], stride=maxpool2_config['stride'])
        self.relu2 = nn.ReLU()

        # Output Layer
        self.output_layer = nn.Linear(fc_layer_size, output_layer_size)

    def forward(self, audio):
        conv1 = self.convlayer1(audio)
        relu1 = self.relu1(conv1)
        relu1 = F.dropout(relu1, 0.5)
        max1 = self.maxpoollayer1(relu1)


        conv2 = self.convlayer2(max1)
        relu2 = self.relu2(conv2)
        relu2 = F.dropout(relu2, 0.5)
        max2 = self.maxpoollayer2(relu2)

        max2 = max2.view(-1, max2.shape[1] * max2.shape[2])
        output_scores = self.output_layer(max2)
        return output_scores, (conv1, conv2)




class CRNN1Layer(nn.Module):

    def __init__(self, conv1_config, maxpool1_config, rnn_config, output_layer_size):
        super(CRNN1Layer, self).__init__()
        # Conv Layer 1
        self.convlayer1 = nn.Conv1d(in_channels=conv1_config['in_channels'],
                                    out_channels=conv1_config['out_channels'],
                                    kernel_size=conv1_config['kernel'],
                                    padding=conv1_config['padding'],
                                    stride=conv1_config['stride'])
        self.maxpoollayer1 = nn.MaxPool1d(kernel_size=maxpool1_config['kernel'],
                                          stride=maxpool1_config['stride'])
        self.relu1 = nn.ReLU()

        # RNN layer
        self.rnn = nn.LSTM(input_size=rnn_config['input_size'],
                           hidden_size=rnn_config['hidden_size'],
                           num_layers=rnn_config['num_layers'],
                           batch_first=rnn_config['batch_first'])

        # Output Layer
        self.output_layer = nn.Linear(rnn_config['hidden_size'], output_layer_size)


    def forward(self, audio):
        conv1 = self.convlayer1(audio)
        relu1 = self.relu1(conv1)
        max1 = self.maxpoollayer1(relu1)

        max1 = max1.transpose(1, 2)
        self.init_hidden(max1.size()[0], max1.size()[1])

        rnn_output, hidden = self.rnn(max1)

        rnn_output = rnn_output[:, -1, :]  # Take only last output
        output_scores = self.output_layer(rnn_output)

        return output_scores, (conv1)


    def init_hidden(self, batch_size, hidden_size):
        self.h0 = Variable(torch.randn(1, batch_size, hidden_size), requires_grad=True)
        self.c0 = Variable(torch.randn(1, batch_size, hidden_size), requires_grad=True)
        if torch.cuda.is_available():
            self.h0 = self.h0.cuda()
            self.c0 = self.c0.cuda()
