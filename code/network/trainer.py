import torch
import torch.nn as nn
import torch.optim as optim
import argparse
import pickle
import sys
import os
sys.path.append("../datautils")

import network
import networkutils
import dataloader




parser = argparse.ArgumentParser(description='Train')
parser.add_argument('--train-only', action='store_true', default=False)
parser.add_argument('--test-only', action='store_true', default=False)
parser.add_argument('--pitch-block-length', type=int, default=10, help='time duration in sec')
parser.add_argument('--model', type=str, default='CNN2Layer')
parser.add_argument('--fc-layer-size', type=int)
parser.add_argument('--epochs', type=int, default=50)
parser.add_argument('--batch-size', type=int, default=32)
parser.add_argument('--learning-rate', type=float, default=0.01)
parser.add_argument('--weight-decay', type=float, default=0.0)
parser.add_argument('--momentum', type=float, default=0.9)
parser.add_argument('--no-cuda', action='store_true', default=False, help='disables GPU training')
parser.add_argument('--augment-data', action='store_true', default=False)
parser.add_argument('--prepare-dataset', action='store_true', default=False)
parser.add_argument('--same-pitch-threshold', type=int, default=225*2, help='Threshold of consecutive pitches before discarding when building a block')
args = parser.parse_args()

args.cuda = not args.no_cuda and torch.cuda.is_available()
print('Running on GPU: ', args.cuda)

pitch_block_size = 225 * args.pitch_block_length
print("Input sample size:\t", pitch_block_size)

# Prepare Datasets and pickle them.
split = {'train': 0.7, 'val': 0.15, 'test': 0.15}
if args.prepare_dataset:
    print("Preparing Datasets...")
    split_dataset, ragadict = dataloader.prepareDataset(pitch_block_size=pitch_block_size,
                                                        split=split,
                                                        same_pitch_threshold=args.same_pitch_threshold,
                                                        augment_data=args.augment_data,
                                                        path='../../data/train/dataset')
    pickle.dump(split_dataset, open("../../data/train/pickled_dataset/dataset.pkl", "wb"))
    pickle.dump(ragadict, open("../../data/train/pickled_dataset/ragadict.pkl", "wb"))
    print("\nDatasets prepared & pickled.")
    quit()


# Load pickled data and create data loaders
print("Loading Pickled Data....")
split_dataset = pickle.load(open("../../data/train/pickled_dataset/dataset.pkl", "rb"))
ragadict = pickle.load(open("../../data/train/pickled_dataset/ragadict.pkl", "rb"))
print("Pickled Data Loaded.")

print("Preparing Data Loaders....")
training_data_loader, val_data_loader, test_data_loader = dataloader.prepareDataLoaders(split_dataset, args.batch_size)
print("Data Loaders prepared.")


# Network Configuration
conv1_config = {
    'in_channels': 1,
    'out_channels': 8,
    'kernel': 25,
    'padding': 0,
    'stride': 5
}
maxpool1_config = {
    'kernel': 3,
    'padding': 0,
    'stride': 3
}

conv2_config = {
    'in_channels': 8,
    'out_channels': 8,
    'kernel': 10,
    'padding': 0,
    'stride': 2
}
maxpool2_config = {
    'kernel': 3,
    'padding': 0,
    'stride': 3
}

rnn_config = {
    'input_size': 8,
    'hidden_size': 8,
    'num_layers': 1,
    'batch_first': True
}

# Calculate and print output sizes of all layers
conv1_size = int((pitch_block_size + 2*conv1_config['padding'] - conv1_config['kernel']) / conv1_config['stride']) + 1
maxpool1_size = int((conv1_size + 2*maxpool1_config['padding'] - maxpool1_config['kernel']) / maxpool1_config['stride']) + 1
cnn1_fc_size = maxpool1_size * conv1_config['out_channels']
conv2_size = int((maxpool1_size + 2*conv2_config['padding'] - conv2_config['kernel']) / conv2_config['stride']) + 1
maxpool2_size = int((conv2_size + 2*maxpool2_config['padding'] - maxpool2_config['kernel']) / maxpool2_config['stride']) + 1
cnn2_fc_size = maxpool2_size * conv2_config['out_channels']
rnn_size = maxpool1_size

print('\nLayer Sizes:')
print('conv1_size:\t', conv1_size)
print('maxpool1_size:\t', maxpool1_size)
print('cnn1_fc_size:\t', cnn1_fc_size)
print('conv2_size:\t', conv2_size)
print('maxpool2_size:\t', maxpool2_size)
print('cnn2_fc_size:\t', cnn2_fc_size)
print('rnn_size:\t', rnn_size)
print()


if args.model == 'CNN1Layer':
    model = network.CNN1Layer(conv1_config=conv1_config,
                        maxpool1_config=maxpool1_config,
                        fc_layer_size=cnn1_fc_size,
                        output_layer_size=len(ragadict))
elif args.model == 'CNN2Layer':
    model = network.CNN2Layer(conv1_config=conv1_config,
                        maxpool1_config=maxpool1_config,
                        conv2_config=conv2_config,
                        maxpool2_config=maxpool2_config,
                        fc_layer_size=cnn2_fc_size,
                        output_layer_size=len(ragadict))
elif args.model == 'CRNN1Layer':
    model = network.CRNN1Layer(conv1_config=conv1_config,
                        maxpool1_config=maxpool1_config,
                        rnn_config=rnn_config,
                        output_layer_size=len(ragadict))
print("Using model:", args.model)
if args.cuda:
    model.cuda()

cross_entropy_loss = nn.CrossEntropyLoss()
sgd_optimizer = optim.SGD(model.parameters(), lr=args.learning_rate)
adam_optimizer = optim.Adam(model.parameters(), lr=args.learning_rate)



if not args.test_only:
    train_loss_cache = []
    val_loss_cache = []
    val_loss = 0
    best_val_loss = 999
    for epoch in range(args.epochs):
        print("\nEpoch " + str(epoch) + ":")
        print("---------------------------------------")
        train_loss, train_accuracy = networkutils.train(model=model,
                                        data_loader=training_data_loader,
                                        loss_criterion=cross_entropy_loss,
                                        optimizer=sgd_optimizer)
        print("Avg training loss:\t" + str(train_loss))
        print("Training accuracy:\t" + str(train_accuracy))
        train_loss_cache.append(train_loss)
        val_loss, val_accuracy = networkutils.evaluate(model=model,
                                                       data_loader=val_data_loader,
                                                       loss_criterion=cross_entropy_loss)
        print("Avg validation loss:\t" + str(val_loss))
        print("Validation accuracy:\t" + str(val_accuracy))
        val_loss_cache.append(val_loss)
        if val_loss < best_val_loss:
            best_val_loss = val_loss
            best_val_accuracy = val_accuracy
            networkutils.saveModel(model)
    print("Training finished.")

    if not os.path.exists('../../results/'):
        os.makedirs('../../results/')

    with open('../../results/results.txt', 'w') as f:
        f.write('Model: ' + args.model + '\n')
        f.write('Train Loss: ' + str(train_loss) + '\n')
        f.write('Train accuracy: ' + str(train_accuracy) + '\n')
        f.write('Best Val Loss: ' + str(best_val_loss) + '\n')
        f.write('Best Val accuracy: ' + str(best_val_accuracy) + '\n\n')

    networkutils.plotLoss(train_loss_cache, val_loss_cache)

if not args.train_only:
    print("\n----TESTING----")
    model = networkutils.loadModel()
    test_loss, test_accuracy = networkutils.evaluate(model=model,
                                      data_loader=test_data_loader,
                                      loss_criterion=cross_entropy_loss)
    print("Test loss:\t" + str(test_loss))
    print("Test accuracy:\t" + str(test_accuracy))



