import os
import glob
import torch
import numpy as np
import torch.utils.data as data


def prepareDataset(pitch_block_size, split, path, same_pitch_threshold, augment_data=False):
    '''
    :param pitch_block_size: (int) Number of samples to be used to form a block
    :param split: (Dict) train, validation and test set split.
    :param path: (str) Location of the raw dataset.
    :param same_pitch_threshold: (int) Max number of consecutive samples of the same pitch to keep in a block. (Zero cleaning)
    :param augment_data: (bool)
    :return: Split datasets , raag ID dict
    '''
    raga_dict = {}
    dataset_root = path
    train_data, train_labels = [], []
    val_data, val_labels = [], []
    test_data, test_labels = [], []

    if os.path.exists(os.path.join(dataset_root, '.DS_Store')):
        os.remove(os.path.join(dataset_root, '.DS_Store'))
    raga_files = os.listdir(dataset_root)

    for i, raga in enumerate(raga_files):
        print('\nRaga', raga)
        print('--------')
        if os.path.exists(os.path.join(dataset_root, raga, '.DS_Store')):
            os.remove(os.path.join(dataset_root, raga, '.DS_Store'))

        raga_dict[raga] = i
        raga_blocks = []
        raga_labels = []
        block_tonic_list = []
        block_count = 0

        for recording in os.listdir(os.path.join(dataset_root, raga)):
            print('Rec', recording)
            pitchfile = glob.glob(os.path.join(dataset_root, raga, recording, "*.pitch"))[0]
            tonicfile = glob.glob(os.path.join(dataset_root, raga, recording, "*.tonic"))[0]
            tonic = __readTonicFile__(tonicfile)
            tonic = __normalizeMIDI__(tonic)
            pitchblocks = __readPitchFile__(pitchfile, pitch_block_size, same_pitch_threshold)
            print('Blocks:', len(pitchblocks))

            for pitchblock in pitchblocks:
                block_tonic_list.append(tonic)
                pitchblock[pitchblock == float('-inf')] = 0  # Set all -inf from midi conversion to 0.
                pitchblock = __normalizeMIDI__(pitchblock)

                if not augment_data:  # Normalize tonics if not gonna augment to all keys
                    pitchblock = __normalizeTonicMIDI__(pitchblock, tonic)

                raga_blocks.append(pitchblock)
                raga_labels.append(raga_dict[raga])
                block_count += 1
        print("Total blocks:", block_count)

        raga_blocks = torch.stack(raga_blocks)
        raga_labels = torch.LongTensor(raga_labels)

        # Split data for each raag
        split_data = __splitData__(raga_blocks, raga_labels, split)
        train_data.extend(split_data[0][0])
        train_labels.extend(split_data[0][1])
        val_data.extend(split_data[1][0])
        val_labels.extend(split_data[1][1])
        test_data.extend(split_data[2][0])
        test_labels.extend(split_data[2][1])

    # Data Augmentation for Train set
    if augment_data:
        train_data, train_labels = __augmentDataset__(train_data, train_labels, tonic)
        print('Training set augmented.')

    if train_data:
        train_data = torch.stack(train_data)
    else:
        train_data = torch.Tensor(train_data)
    if val_data:
        val_data = torch.stack(val_data)
    else:
        val_data = torch.Tensor(val_data)
    if test_data:
        test_data = torch.stack(test_data)
    else:
        test_data = torch.Tensor(test_data)

    train_labels = torch.LongTensor(train_labels)
    val_labels = torch.LongTensor(val_labels)
    test_labels = torch.LongTensor(test_labels)

    train_dataset = data.TensorDataset(train_data, train_labels)
    val_dataset = data.TensorDataset(val_data, val_labels)
    test_dataset = data.TensorDataset(test_data, test_labels)

    print("\nDataset Sizes:")
    print('Size of train dataset:\t', len(train_dataset))
    print('Size of val dataset:\t', len(val_dataset))
    print('Size of test dataset:\t', len(test_dataset))
    print()

    split_dataset = train_dataset, val_dataset, test_dataset
    return split_dataset, raga_dict


def __readTonicFile__(tonicfile):
    with open(tonicfile, 'r') as tonicfile:
        tonic = float(tonicfile.readlines()[0])
        tonic = __convertToMIDI__(tonic)
    return tonic


def __readPitchFile__(pitchfile, pitch_block_size, same_pitch_threshold):
    '''
    Reads pitch files, removes zeros and forms blocks of pitch values.
    '''
    with open(pitchfile, 'r') as pitchfile:
        pitch_blocks = []
        pitch_list = []

        same_pitch_counter = 0
        #previous_pitch = 0
        for i, line in enumerate(pitchfile.readlines()):
            timestamp, pitch = line.split('\t')
            pitch = float(pitch)

            pitch = __convertToMIDI__(pitch)

            #if pitch == previous_pitch:
            if pitch == float('-inf'):  # remove only zeros
                same_pitch_counter += 1
            else:
                same_pitch_counter = 0
            #previous_pitch = pitch

            if same_pitch_counter < same_pitch_threshold:
                if len(pitch_list) < pitch_block_size:
                    pitch_list.append(pitch)
                else:
                    pitch_blocks.append(torch.Tensor(pitch_list))
                    pitch_list = []

        return pitch_blocks


def __convertToMIDI__(pitch):
    midi = 69 + 12 * np.log2(pitch/440)
    return midi


def __normalizeMIDI__(block):
    return block / 128


def __normalizeTonicMIDI__(block, tonic):
    return block - tonic


def __augmentDataset__(input, labels, tonic):
    augmented_inputs = []
    augmented_labels = []
    for i in range(len(input)):
        augmented_blocks = __augmentPitchBlock__(input[i], tonic)
        augmented_inputs.extend(augmented_blocks)
        augmented_labels.extend(labels[i] for x in range(12))  # Add 12 labels for 12 keys of augmented data
    return augmented_inputs, augmented_labels


def __augmentPitchBlock__(pitch_block, tonic):
    tonic = round(tonic)
    pitch_block[pitch_block == 0] = float('-inf')
    augmented_pitch_blocks = []
    for key in range(60, 72):
        key = __normalizeMIDI__(key)
        new_block = pitch_block + (key - tonic)
        new_block[new_block == float('-inf')] = 0   # set silent pitch values to 0
        new_block = __normalizeMIDI__(new_block)
        augmented_pitch_blocks.append(new_block)
    return augmented_pitch_blocks


def __splitData__(inputs, labels, split):

    data_size = len(inputs)
    train_split = (0, int(split['train'] * data_size))
    val_split = (train_split[1], train_split[1] + int(split['val'] * data_size))
    test_split = (val_split[1], val_split[1] + int(split['test'] * data_size))

    np.random.seed(1)
    indices = list(range(data_size))
    np.random.shuffle(indices)

    train_indices = indices[train_split[0]: train_split[1]]
    val_indices = indices[val_split[0]: val_split[1]]
    test_indices = indices[test_split[0]: test_split[1]]

    train_set = inputs[train_indices], labels[train_indices]
    val_set = inputs[val_indices], labels[val_indices]
    test_set = inputs[test_indices], labels[test_indices]

    print('Split sizes:', len(train_indices), len(val_indices), len(test_indices))
    return train_set, val_set, test_set


def prepareDataLoaders(split_dataset, batch_size):

    train_sampler = data.RandomSampler(split_dataset[0])
    val_sampler = data.RandomSampler(split_dataset[1])
    test_sampler = data.RandomSampler(split_dataset[2])

    train_data_loader = data.DataLoader(split_dataset[0], batch_size=batch_size, sampler=train_sampler)
    val_data_loader = data.DataLoader(split_dataset[1], batch_size=batch_size, sampler=val_sampler)
    test_data_loader = data.DataLoader(split_dataset[2], batch_size=batch_size, sampler=test_sampler)

    return train_data_loader, val_data_loader, test_data_loader
